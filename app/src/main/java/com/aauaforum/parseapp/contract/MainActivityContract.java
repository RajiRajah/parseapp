package com.aauaforum.parseapp.contract;

import android.content.Context;

/**
 * Created by Rajah on 11/6/2017.
 */

public interface MainActivityContract {
    interface View{
        Context getContext();
        Context getAppContext();
        void showMessage(String msg);
    }

    interface Presenter{
        void signIn(String username, String password);
        void signUp(String username, String password);
    }
}
