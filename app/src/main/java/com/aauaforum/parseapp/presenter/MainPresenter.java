package com.aauaforum.parseapp.presenter;

import android.app.ProgressDialog;

import com.aauaforum.parseapp.MainActivity;
import com.aauaforum.parseapp.contract.MainActivityContract;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by Rajah on 11/6/2017.
 */

public class MainPresenter implements MainActivityContract.Presenter{

    private MainActivityContract.View mView;
    ProgressDialog progressDialog;

    private String details = "";

    public MainPresenter(MainActivityContract.View mView) {
        this.mView = mView;

        progressDialog = new ProgressDialog(mView.getContext());
        progressDialog.setMessage("Please Wait");
    }

    @Override
    public void signIn(String username, String password) {
        progressDialog.setTitle("Logging in");
        progressDialog.show();
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    progressDialog.dismiss();
                    details = "Welcome " + user.getUsername();
                    mView.showMessage(details);
                } else {
                    progressDialog.dismiss();
                    details = "Error " + e.getMessage();
                    mView.showMessage(details);
                }
            }
        });

    }

    @Override
    public void signUp(String username, String password) {
        progressDialog.setTitle("Registering..");
        progressDialog.show();
        ParseUser user = new ParseUser();
        user.setUsername(username);
        user.setPassword(password);
        user.saveInBackground();
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null){
                    progressDialog.dismiss();
                    details = "Successfully Registered!";
                    mView.showMessage(details);
                }else {
                    progressDialog.dismiss();
                    details = "Not Registered!" + e.getMessage();
                    mView.showMessage(details);
                }
            }
        });
    }
}
