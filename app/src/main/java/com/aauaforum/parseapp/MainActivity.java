package com.aauaforum.parseapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aauaforum.parseapp.contract.MainActivityContract;
import com.aauaforum.parseapp.presenter.MainPresenter;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MainActivityContract.View {

    @BindView(R.id.username)
    EditText userName;
    @BindView(R.id.password)
    EditText passWord;

    @BindView(R.id.sign_in)
    Button SignIn;
    @BindView(R.id.sign_up)
    Button signUp;

    private MainPresenter mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Parse.initialize(this);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        mPresenter = new MainPresenter(this);

        SignIn.setOnClickListener(this);
        signUp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in:
                mPresenter.signIn(userName.getText().toString(), passWord.getText().toString());
                break;
            case R.id.sign_up:
                mPresenter.signUp(userName.getText().toString(), passWord.getText().toString());
                break;
        }
    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return this.getApplicationContext();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }
}
